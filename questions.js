module.exports = [
  {
    "input_name": "fav_mov",
    "answers": [
      {
        "value": 0,
        "jediPoints": 1,
        "sithPoints": 0
      },
      {
        "value": 1,
        "jediPoints": 0,
        "sithPoints": 1
      },
      {
        "value": 2,
        "jediPoints": 0,
        "sithPoints": 3
      },
      {
        "value": 3,
        "jediPoints": 2,
        "sithPoints": 0
      },
      {
        "value": 4,
        "jediPoints": 0,
        "sithPoints": 2
      },
      {
        "value": 5,
        "jediPoints": 3,
        "sithPoints": 0
      },
      {
        "value": 6,
        "jediPoints": 0,
        "sithPoints": 0
      }
    ]
  },
  {
    "input_name": "fav_jedi",
    "answers": [
      {
        "value": 0,
        "jediPoints": 1,
        "sithPoints": 0
      },
      {
        "value": 1,
        "jediPoints": 1,
        "sithPoints": 0
      },
      {
        "value": 2,
        "jediPoints": 2,
        "sithPoints": 0
      },
      {
        "value": 3,
        "jediPoints": 0,
        "sithPoints": 2
      }
    ]
  },
  {
    "input_name": "fav_sith",
    "answers": [
      {
        "value": 0,
        "jediPoints": 0,
        "sithPoints": 2
      },
      {
        "value": 1,
        "jediPoints": 0,
        "sithPoints": 1
      },
      {
        "value": 2,
        "jediPoints": 0,
        "sithPoints": 1
      },
      {
        "value": 3,
        "jediPoints": 2,
        "sithPoints": 0
      }
    ]
  },
  {
    "input_name": "fav_planet",
    "answers": [
      {
        "value": 0,
        "jediPoints": 1,
        "sithPoints": 0
      },
      {
        "value": 1,
        "jediPoints": 2,
        "sithPoints": 0
      },
      {
        "value": 2,
        "jediPoints": 0,
        "sithPoints": 1
      },
      {
        "value": 3,
        "jediPoints": 0,
        "sithPoints": 2
      }
    ]
  },
  {
    "input_name": "force_select",
    "answers": [
      {
        "value": 0,
        "jediPoints": 2,
        "sithPoints": 0
      },
      {
        "value": 1,
        "jediPoints": 0,
        "sithPoints": 2
      }
    ]
  }
];