var config = {};

config.sith = {
    title: "Sith Happens!",
    message: `Fear is the path to the dark side. Fear leads to anger.
        Anger leads to hate. Hate leads to suffering. Remember
        this as you become the apprentice of the dark side.`
};
config.jedi = {
    title: "The last Jedi!",
    message: `You are the Chosen One. You have brought balance to this
    world. Stay on this path, and you will do it again for
    galaxy. But beware your heart.`
};
config.sithJedi = {
    title: "Who am I !?",
    message: `You have a personality disorder, just like Anakin/Vader
        when he tries to kill Luke.`
};

module.exports = config;