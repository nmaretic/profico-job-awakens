var express = require('express');
var bodyParser = require('body-parser');
var service = require('./service.js');

var app = express();

app.use(bodyParser.json());

app.post('/testme', function (req, res) {
  service.validateAnswersFormat(req.body);
  res.status(200).json(service.getResults(req.body));
});

app.use(function (err, req, res, next) {
  res.status(200).send({error: err.message});
})

app.listen(5000, function () {
  console.log('node Server is running');
})