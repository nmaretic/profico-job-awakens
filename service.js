var questions = module.require('./questions');
var config = module.require('./config');
var _ = module.require('lodash');

module.exports = {
  getResults: getResults,
  validateAnswersFormat: validateAnswersFormat
};

function validateAnswersFormat(userAnswers) {

  var questionsList = questions.map(x => x.input_name);
  var answersList = Object.keys(userAnswers);
  var difference = _.difference(answersList, questionsList);
  if (difference.length > 0) {
    throw new Error(`We didn't ask you for ${difference}`);
  }
}

function getResults(userAnswers) {

  var score = calculateScore(userAnswers);
  var result;
  if (score < 0) {
    result = config.sith;
  } else if (score == 0) {
    result = config.sithJedi;
  } else {
    result = config.jedi;
  }

  return result;
}

function calculateScore(userAnswers) {

  var jediPoints = 0,
    sithPoints = 0;
  var answer;
  for (question of questions) {
    answer = userAnswers[question.input_name];
    if (!question.answers[answer]) {
      throw new Error(`Looks like you found a new answer for ${question.input_name}. 
Good for you, but could you please pick one from 0-${question.answers.length-1}??`);
    }
    jediPoints += question.answers[answer].jediPoints;
    sithPoints += question.answers[answer].sithPoints;
  }

  return jediPoints - sithPoints;
}